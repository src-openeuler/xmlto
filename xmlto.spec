#spec from source code(%{name}-%{version}.tar.bz2)

%bcond_with tex
%bcond_with xhtml

Name:     xmlto
Summary:  A tool for converting XML files to various formats.
Version:  0.0.29
Release:  1
License:  GPL-2.0-or-later
URL:      https://pagure.io/xmlto/
Source0:  https://releases.pagure.org/%{name}/%{name}-%{version}.tar.bz2

BuildRequires: docbook-xsl libxslt util-linux flex gcc
BuildRequires: autoconf automake libtool

# We rely entirely on the DocBook XSL stylesheets!
Requires: docbook-xsl libxslt docbook-dtds util-linux flex

%description
This is a package for converting XML files to various formats using XSL
stylesheets.

%if %{with tex}
%package tex
Group: Applications/System
Summary: A set of xmlto backends with TeX requirements
# For full functionality, we need passivetex.
Requires: passivetex >= 1.11
# We require main package
Requires: xmlto = %{version}-%{release}
BuildArch: noarch

%description tex
This subpackage contains xmlto backend scripts which do require
PassiveTeX/TeX for functionality.
%endif

%if %{with xhtml}
%package xhtml
Group: Applications/System
Summary: A set of xmlto backends for xhtml1 source format
# For functionality we need stylesheets xhtml2fo-style-xsl
Requires: xhtml2fo-style-xsl
# We require main package
Requires: xmlto = %{version}-%{release}
BuildArch: noarch

%description xhtml
This subpackage contains xmlto backend scripts for processing
xhtml1 source format.
%endif

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -fi
%configure
%make_build

%install
%make_install

[ -d %{buildroot}%{_datadir}/xmlto/xsl ] || \
  mkdir %{buildroot}%{_datadir}/xmlto/xsl

%check
%make_build check

%files
%license COPYING
%doc ChangeLog AUTHORS.md
%{_bindir}/*
%{_datadir}/xmlto
%exclude %{_datadir}/xmlto/format/fo/dvi
%exclude %{_datadir}/xmlto/format/fo/ps
%exclude %{_datadir}/xmlto/format/fo/pdf
%exclude %{_datadir}/xmlto/format/xhtml1

%if %{with tex}
%files tex
%{_datadir}/xmlto/format/fo/dvi
%{_datadir}/xmlto/format/fo/ps
%{_datadir}/xmlto/format/fo/pdf
%endif

%if %{with xhtml}
%files xhtml
%dir %{_datadir}/xmlto/format/xhtml1/
%{_datadir}/xmlto/format/xhtml1/*
%endif

%files help
%doc README.md NEWS.md
%{_mandir}/*/*

%changelog
* Wed Oct 30 2024 Funda Wang <fundawang@yeah.net> - 0.0.29-1
- update to 0.0.29

* Thu Mar 12 2020 chengquan<chengquan3@huawei.com> - 0.0.28-15
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Remove some default installation packages

* Mon Feb 17 2020 chengquan<chengquan3@huawei.com> - 0.0.28-14
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Add necessary BuildRequire

* Wed Dec 4 2019 shenyangyang<shenyangyang4@huawei.com> - 0.0.28-13
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise comments

* Sat Sep 21 2019 shenyangyang<shenyangyang4@huawei.com> - 0.0.28-12
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise description

* Wed Sep 4 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.0.28-11
- strenthen spec 

* Wed Nov 18 2015 Ondrej Vasik <ovasik@redhat.com>
- New version 0.0.28
- fix broken temp files removal
- do not detect links browser as elinks

* Tue Nov 10 2015 Ondrej Vasik <ovasik@redhat.com>
- New version 0.0.27
- remove several bashisms in scripts
- add new option --profile for preprocessing documents
  with profiling stylesheet
- fix several potential crashes in xmlif (found by static analysis)

* Sat Apr 04 2014 Ondrej Vasik <ovasik@redhat.com>
- New version 0.0.26
- fix build with automake 1.13+
- fix warning in searchpath option

* Fri Dec 02 2011 Ondrej Vasik <ovasik@redhat.com>
- New version 0.0.25
- fix the fop.extensions setting (based on fop version)
- fix handling of external data objects with fop
  (deb #568894)

* Thu Jul 14 2011 Ondrej Vasik <ovasik@redhat.com>
- New version 0.0.24
- use backend extensions by default (--noextensions
  to disable it)
- basic support for epub format
- workaround passivetex limitation for chapter titles
  starting with L (rhbz #526273)

* Mon Sep 21 2009 Ondrej Vasik <ovasik@redhat.com>
- New version 0.0.23
- added autodetection for more common tools like
  gnu cp or tail
- added option --noautosize to prevent overriding
  of user-defined or system-default paper size
- use shell built-in 'type -t' instead of 'which'
  utility for detection of file availability

* Wed Mar 25 2009 Ondrej Vasik <ovasik@redhat.com>
- New version 0.0.22
- added xhtml1 source format support
- autodetection for tools/program paths, consolidated
  error code handling
- fixed libpaper cleanup, validation check now uses
  --noent and --nonet option
- fixed broken --stringparam option

* Fri Jun 20 2008 Ondrej Vasik <ovasik@redhat.com>
- New version 0.0.21
- added dblatex experimental support
- non-mandatory libpaper support
- fixed issue of cp -a option on non-gnu systems

* Tue Jan 15 2008 Ondrej Vasik <ovasik@redhat.com>
- New version 0.0.20
- fop experimental support
- possibility to read stylesheet from STDIN, using
  recursive cp in docbook formats, preparations
  for other source formats

* Mon Nov 19 2007 Ondrej Vasik <ovasik@redhat.com>
- New version 0.0.19
- License GPLv2 , changes since last comment in NEWS

* Fri May 23 2003 Tim Waugh <twaugh@redhat.com>
- Be sure to create the xsl directory.
- README.docbook-xsl is no longer shipped.

* Wed Oct  9 2002 Tim Waugh <twaugh@redhat.com>
- Build requires docbook-xsl >= 1.56.0.

* Sun Oct  6 2002 Tim Waugh <twaugh@redhat.com>
- Remove 'BuildArch: noarch' now that we ship a compiled object.
- Run tests.
- Ship xmlif.
- Build requires docbook-xsl >= 1.52.0.

* Fri Aug 30 2002 Tim Waugh <twaugh@redhat.com>
- Bump docbook-xsl requirement to 1.52.0 for manpages.

* Fri Aug  2 2002 Tim Waugh <twaugh@redhat.com>
- The archive is now distributed in .tar.bz2 format.

* Fri Jan 25 2002 Tim Waugh <twaugh@redhat.com>
- Require the DocBook DTDs.

* Fri Jan 18 2002 Tim Waugh <twaugh@redhat.com>
- Ship README.docbook-xsl.

* Fri Nov 23 2001 Tim Waugh <twaugh@redhat.com>
- Initial spec file.
